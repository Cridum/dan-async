# `async`/`await`

«async/await» - специальный синтаксис для работы с промисами.

<hr />

## Асинхронные функции

Объявление `async function(){}` определяет асинхронную функцию, которая возвращает объект `AsyncFunction`.

У слова `async` один простой смысл: эта функция всегда возвращает промис. Значения других типов оборачиваются в завершившийся успешно промис автоматически.

```js

const f = async () => {
    // throw new Error('Some error');
    return 'Some value'
}

console.log(f())

```


Функция `async` может содержать выражение `await`, которое приостанавливает выполнение функции `async` и ожидает ответа от переданного `Promise`, затем возобновляя выполнение функции `async` и возвращая полученное значение.

Ключевое слово `await` допустимо только в асинхронных функциях. В другом контексте вы получите ошибку `SyntaxError`.


```js

const resolveAfter2Seconds = () => new Promise(resolve => {
    setTimeout(() => {
        resolve('Value after 2 seconds');
    }, 2000);
});

const getValue = async () => {
    const result = await resolveAfter2Seconds();

    
    console.log(result);
}

getValue();

```

```js

const getRandomUser = async () => {
    const result = await fetch('https://randomuser.me/api/').then(res => res.json());

    console.log(result);
    return result.results[0];
}

getRandomUser();

```

<hr />



## `await` только внутри асинхронной функции

Мы не можем использовать `await` вне асинхронной функции. Это частая ошибка начинающего разработчика. 

```js

// Будет ошибка
const result = await fetch('https://randomuser.me/api/').then(res => res.json());

const { results: [user] } = results;

```

На помощь приходят `async IIFE`

```js

(async () => {
    const result = await fetch('https://randomuser.me/api/').then(res => res.json());

    const { results: [user] } = results;
    console.log(user)
})()

```
<hr />

## Обработка ошибок

Когда промис завершается успешно, `await promise` возвращает результат. Когда завершается с ошибкой – будет выброшено исключение. Как если бы на этом месте находилось выражение `throw`.

При работе с асинхронными функциями принято использовать конструкцию `try/catch`;

```js

const rejectAfter2Seconds = () => new Promise((_, reject) => {
    setTimeout(() => {
        reject(new Error('Some Critical Error'))
    }, 2000)
})

const getSumWithAsyncError = async () => {
    try {
        const result = await rejectAfter2Seconds();
        console.log(result)
    } catch (error) {
        console.warn(`HERE IS EEROR!!: ${error}`)
    }


    const x = 1;
    const y = 2;
    console.log('SUM', x + y);
}

getSumWithAsyncError();

```

При этом никто не запрещает нам использовать `catch` из функционала промисов;

```js

const rejectAfter2Seconds = () => new Promise((_, reject) => {
    setTimeout(() => {
        reject(new Error('Some Critical Error'))
    }, 2000)
})

const getSumWithAsyncError = async () => {
    const result = await rejectAfter2Seconds().catch(error => console.warn('HERE IS EEROR!!: ', error));
    console.log(result)
    
    const x = 1;
    const y = 2;
    console.log('SUM', x + y);
}

getSumWithAsyncError();

```

<hr />

## `await` и `Promise.all`

```js

/**
 * @param value {string||number}
 * @param timeot {number}
 * @return {Promise<unknown>}
 */
const resolveAfterSomeSeconds = (value, timeot) => new Promise(resolve => {
    setTimeout(() => {
        resolve(value);
    }, timeot);
});

const getValueSum = async () => {
    const a = await resolveAfterSomeSeconds(20, 2000);
    const b = await resolveAfterSomeSeconds(30, 2000);
    const sum = a + b;
    console.log(sum);
}

getValueSum();

```

<blockquote>
    Таймеры через <code>await</code> запускаются не параллельно, а друг за другом - такая конструкция не означает автоматического использования <code>Promise.all</code>. Если два или более <code>Promise</code> должны разрешаться параллельно, следует использовать <code>Promise.all</code>.
</blockquote>

Но `await` отлично работает в связке с `Promise.all`

```js

/**
 * @param value {string||number}
 * @param timeot {number}
 * @return {Promise<unknown>}
 */
const resolveAfterSomeSeconds = (value, timeot) => new Promise(resolve => {
    setTimeout(() => {
        resolve(value);
    }, timeot);
});

const getValueSum = async () => {
    const results = await Promise.all(resolveAfterSomeSeconds(20, 2000), resolveAfterSomeSeconds(30, 3000))
    const [a, b] = results;
    const sum = a + b;
    console.log(sum);
}

getValueSum();

```

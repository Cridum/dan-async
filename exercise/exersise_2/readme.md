[API](https://api.apilayer.com/currency_data)
[API key](tucMdWDst5StXp5CXaIa6AoXYJUemziK)

Побудувати конвертор валют користуючись існуючим API - https://apilayer.com/marketplace/currency_data-api
Приклад https://finance.liga.net/currency/converter

1. Використовуючи async /await JS вивести список доступних валют у форматі 2х селектів 

2. Підключити інпут в якому буде сума валюти яку ми хочемо розрахувати

3. Використовуючи async /await JS отримати live співвідношення валют та розрахувати необхідну суму

4. Додати функціонал вибору певної дати на яку також можна перевірити курс валют

import { selectElem } from './selectElem.js';

export function calculateCurrency(result, res) {
    const input = selectElem('#amount');
    const element = selectElem(res);

    const key = Object.keys(result.quotes)[0];
    const sum = input.value * result.quotes[key];
    const sumRounded = sum.toFixed(2);
    element.innerText = sumRounded;
}
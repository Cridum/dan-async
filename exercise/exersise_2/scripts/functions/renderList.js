import { selectElem } from './selectElem.js';

export function renderList(result, el) {
    const { currencies } = result;
    const element = selectElem(el);
    
    Object.keys(currencies).forEach(currency => {
        const option = document.createElement('option');
        option.innerText = currency;
        element.append(option);
    });
}
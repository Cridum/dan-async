import { requestOptions } from './secret.js';
import { renderList } from '../functions/renderList.js';
import { calculateCurrency } from '../functions/calculateCurrency.js';


const CURRENCY_API = 'https://api.apilayer.com/currency_data/';

export default function fetchCurrency(endpoint, selector, obj={}) {
    const {date, source, currencies} = obj;
    let FULL_URL = CURRENCY_API;
    let actionFunc = null;

    switch (endpoint) {
        case 'list': 
            FULL_URL += endpoint;
            actionFunc = renderList;
        break;
        case 'live': FULL_URL += `${endpoint}?source=${source}&currencies=${currencies}`;
        actionFunc = calculateCurrency;
        break;
        default: FULL_URL += `${endpoint}?date=${date}&source=${source}&currencies=${currencies}`;
        actionFunc = calculateCurrency;
    }
    return fetch(`${FULL_URL}`, requestOptions)
        .then(response => response.json())
        .then(result => {
             actionFunc(result, selector);
        })
        .catch(error => console.log('error', error));
}





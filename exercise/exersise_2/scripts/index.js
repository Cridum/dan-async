import fetchCurrency from './API/api.js';


async function currencyList(selector) {

    await fetchCurrency('list', selector);

}

function calculate() {
    const btn = document.querySelector('.sub__btn');
    const source = document.querySelector('#source');
    const dest = document.querySelector('#dest');
    const date = document.querySelector('#date');

    btn.addEventListener('click', async () => {
        let obj = {
            date: date.value,
            source: source.value,
            currencies: dest.value
        };
        if (!date.value) {
 
            await fetchCurrency('live', '.res', obj);

        } else {

            await fetchCurrency('historical', '.res', obj);
        }


    })
}


currencyList('#source');
currencyList('#dest');
calculate();


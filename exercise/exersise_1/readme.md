# Users

Використовуючи `async/await` отримати по вказаному URL об'єкти користувачів кількість на вибір

```js

const url = 'https://randomuser.me/api';

```

<blockquote>
    Сервер повертає помилки на більшість запитів. Намагайтесь отримати 5-6 позитивних відповідей.
</blockquote>

Відмалювати в `<div class="container"></div>` карточки типу:

```html

<div class="user">
    <img src="{picture}" alt="{name}">
    <span>Name: {full name}</span>
    <span>Gender: {gender}</span>
    <span>City: {city}</span>
    <span>Tel: {phone}</span>
    <span>Email: {email}</span>
</div>

```

P.S. Телефон та пошта мають бути клікабельними лінками й, відповідно відкривати функціонал набору номера або відправки листа.

Якщо немає жодного позитивної відповіді сервера - вивести на екран повідомлення.

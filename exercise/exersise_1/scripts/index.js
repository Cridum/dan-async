const url = 'https://randomuser.me/api';

class User {
	constructor({picture, name, fullName, gender, city, phone, email}) {
		this.picture = picture;
		this.name = name;
		this.fullName = fullName;
		this.gender = gender;
		this.city = city;
		this.phone = phone;
		this.email = email;
	}

	render(selector) {
		document.querySelector(selector).insertAdjacentHTML('beforeend',`
		<div class="user">
				<img src="${this.picture}" alt="${this.name}">
				<span>Name: ${this.fullName}</span>
				<span>Gender: ${this.gender}</span>
				<span>City: ${this.city}</span>
				<a href="tel:${this.phone}">Tel: ${this.phone}</a">
				<a href="mailto:${this.email}">Email: ${this.email}</a>
		</div>
		`)
	}
}

const userAdapter = ({name, picture, gender, location, phone, email}) => ({
	name: name.first,
	fullName: `${name.title} ${name.first} ${name.last}`,
	picture: picture.medium,
	gender,
	city: location.city,
	phone,
	email
});

const getUsers = async () => {
	// const user = await fetch(url).then(response => response.json());
	try {
		const promiseArray = new Array(20).fill(null).map(el => fetch(url).then(response => response.json()));
		const usersResponse = await Promise.allSettled(promiseArray);

        return usersResponse;
	}
	catch(err) {
		console.error(err);
	}
}

const drawUsers = async () => {
    const usersResponse = await getUsers();

    usersResponse.forEach(({status, value: {results}}) => {
        if (status === 'fulfilled') {
            new User(userAdapter(results[0])).render('.container');
        }
    });
}

drawUsers();